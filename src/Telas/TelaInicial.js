import { CampoTexto } from "../components/CampoTexto/CampoTexto";
import { BaseLogin } from "../components/BaseLogin/BaseLogin";
import { ProvedorTema } from "../components/ProvedorTema/ProvedorTema";
import { Estilos } from "../components/EstilosGlobais/Estilos";
import React from "react";
import { Botao } from "../components/Botao/Botao";


function App() {
  return (
    <ProvedorTema>
      <Estilos />
      <BaseLogin texto="BEM VINDO AO ORIENTE" botao="Enviar" tamanho="100vh" variante="body1" componente="h2" tipo="submit">
        <form>
        <CampoTexto label="TIA/DRT" />
        <CampoTexto label="SENHA" tipo="password"/>

        <div style={{padding: "10px 0 10px 0"}}>
        
        <a href="" style={{alignSelf: "right", marginLeft: "0", marginRight: "auto"}}> Esqueci minha senha </a>
        </div>

        <div id="botao" style={{width: "100%", display: "flex", justifyContent: "right"}}>
          <Botao text="Enviar" tipo="button"/>
       </div>
        </form>
      </BaseLogin>
      
    </ProvedorTema>
  );
}

export default App;
