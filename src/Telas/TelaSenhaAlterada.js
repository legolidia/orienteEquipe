import { BaseLogin } from "../components/BaseLogin/BaseLogin";
import { ProvedorTema } from "../components/ProvedorTema/ProvedorTema";
import { Estilos } from "../components/EstilosGlobais/Estilos";
import { Botao } from "../components/Botao/Botao";

function AlteracaoSenha() {
  return (
    <ProvedorTema>
      <Estilos />
      <BaseLogin texto="Senha alterada com sucesso!!!" botao="Home" tamanho="60vh" variante="body1" componente="h3">
        <Botao text="Home" />
      </BaseLogin>
    </ProvedorTema>
  );
}

export default AlteracaoSenha;
