import styled from "@emotion/styled"
import { useState } from "react";

const InputEstilizada = styled.input`
    width: 100%;
    box-sizing: border-box;
    margin-bottom: ${props => props.theme.espacamentos.s};
    background: ${props => props.theme.cores.brancas}; 
    background-color: ${props => props.theme.cores.secundarias.a};
    border: 0;
    border-radius: ${props => props.theme.espacamentos.s};
    height: 40px;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    padding: 10px;
    min-width: 400px;
    max-width: 60vw;

    ::placeholder {
      color: ${props => props.theme.cores.neutras.a};
      font-family: ${props => props.theme.fontFamily};
      padding: 0 5px;
    }
`

export const CampoTexto = ({ label, tipo }) => {
  const [placeholder, setPlaceholder] = useState(label);

  const handleClick = () => {
    setPlaceholder('');
  };

  const handleBlur = () => {
    if (placeholder === '') {
      setPlaceholder(label);
    }
  };

  return (
    <InputEstilizada
      type={tipo}
      placeholder={placeholder}
      onClick={handleClick}
      onBlur={handleBlur}
    />
  );
};