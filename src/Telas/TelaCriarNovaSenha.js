import { BaseLogin } from "../components/BaseLogin/BaseLogin";
import { ProvedorTema } from "../components/ProvedorTema/ProvedorTema";
import { Estilos } from "../components/EstilosGlobais/Estilos";
import { CampoTexto } from '../components/CampoTexto/CampoTexto';
import { Botao } from "../components/Botao/Botao";

function CriarSenha() {
  return (
    <ProvedorTema>
      <Estilos />
      <BaseLogin texto="Digite sua nova senha abaixo e confirme" botao="Confirmar" tamanho="80vh" variante="body1" componente="h3">
        <CampoTexto label="NOVA SENHA" />
        <CampoTexto label="CONFIRMAR SENHA" />

        <div id="botao" style={{width: "100%", display: "flex", justifyContent: "right"}}>

        <Botao text="Confirmar" />
        </div>

      </BaseLogin>
    </ProvedorTema>
  );
}

export default CriarSenha;
