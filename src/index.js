import React from 'react';
import ReactDOM from 'react-dom/client';

import reportWebVitals from './reportWebVitals';
import App from './Telas/TelaInicial';
import SolicitacaoSenha from './Telas/TelaSolicitacaoSenha';
import LinkSenha from './Telas/TelaConfirmacaoSenhaNova';
import AlteracaoSenha from './Telas/TelaSenhaAlterada';
import CriarSenha from './Telas/TelaCriarNovaSenha';





const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <SolicitacaoSenha />
  </React.StrictMode>
);


reportWebVitals();
