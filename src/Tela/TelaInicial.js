import { CampoTexto } from "../components/CampoTexto/CampoTexto";
import { BaseLogin } from "../components/BaseLogin/BaseLogin";
import { ProvedorTema } from "../components/ProvedorTema/ProvedorTema";
import { Estilos } from "../components/EstilosGlobais/Estilos";


function App() {
  return (
    <ProvedorTema>
      <Estilos />
      <BaseLogin texto="BEM VINDO AO ORIENTE" botao="Enviar" tamanho="90vh" variante="body1" componente="h2">
        <CampoTexto label="TIA/DRT" />
        <CampoTexto label="SENHA" />
        <div style={{alignSelf: "right", marginTop: "10px", marginLeft: "0", marginRight: "273px", marginBottom: "10px", padding: "2 2 2 2", fontSize: "15px"}}>
        <a href=""> Esqueci minha senha </a>
        </div>


      </BaseLogin>
    </ProvedorTema>
  );
}

export default App;
