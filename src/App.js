import React from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import TelaInicial from "./Telas/TelaInicial";
import SolicitacaoSenha from "./Telas/TelaSolicitacaoSenha";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<TelaInicial />} />
        <Route path="/esqueciSenha" element={<SolicitacaoSenha />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
