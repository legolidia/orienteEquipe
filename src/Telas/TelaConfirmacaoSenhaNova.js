import { BaseLogin } from "../components/BaseLogin/BaseLogin";
import { ProvedorTema } from "../components/ProvedorTema/ProvedorTema";
import { Estilos } from "../components/EstilosGlobais/Estilos.jsx";
import { Botao } from "../components/Botao/Botao";

function LinkSenha() {
  return (
    <ProvedorTema>
      <Estilos />
      <BaseLogin texto="Link enviado para e-mail cadastrado." botao="Home" tamanho="60vh" variante="body1" componente="h3">
        <Botao text="Home" />
      </BaseLogin>
    </ProvedorTema>
  );
}

export default LinkSenha;
