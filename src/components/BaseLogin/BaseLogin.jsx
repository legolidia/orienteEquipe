import styled from '@emotion/styled';
import { Card } from '../Card/Card';
import { Tipografia } from '../Tipografia/Tipografia';
import { Botao } from '../Botao/Botao';
import React, { useEffect, useState } from "react";
import { Cabecalho } from '../Cabecalho/Cabecalho';

const Base = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: url("../imagens/Imagens/image 3.png");
  background-size: cover;
  box-shadow: inset 0px 4px 222px rgba(0, 0, 0, 0.25);
  background-repeat: no-repeat;
  min-height: 100vh;
  overflow-y: auto;
  position: relative;
`

const Conteudo = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10px;
`;

export const BaseLogin = ({ texto, children, tamanho, variante, componente}) => {
  const [isOverflow, setIsOverflow] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      const windowHeight = window.innerHeight;
      const baseHeight = document.getElementById("base").offsetHeight;
      setIsOverflow(baseHeight > windowHeight);
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Base id="base" style={{ overflowY: isOverflow ? "auto" : "initial" }}>
      <Cabecalho />
      <Conteudo>
        <Card>
          <img src="./imagens/Imagens/image 2.png" alt="logo" height={tamanho} />

          <Tipografia variante={variante} componente={componente}>
            {texto}
          </Tipografia>

          {children}

          
        </Card>
      </Conteudo>
    </Base>
  );
}
