import styled from "@emotion/styled"

const DivEstilizada = styled.div`
  padding: 20px;
  border: 0;
  border-radius: ${props => props.theme.espacamentos.s};
  background-color: #FFFFFF;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.56);
  height: auto;
  max-width: 590px;
  max-height: 600px;
  margin: 10px;
  position: relative; 
  z-index: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 20vh;
  & > * {
    margin-bottom: 13px;
    margin-top: 13px;

  }
  @media (max-width: 768px) {
    margin-top: 20vh;
   }

`

export const Card = ({ children }) => {
  return (<DivEstilizada>
    {children}
  </DivEstilizada>)
}