import { CampoTexto } from "../components/CampoTexto/CampoTexto";
import { BaseLogin } from "../components/BaseLogin/BaseLogin";
import { ProvedorTema } from "../components/ProvedorTema/ProvedorTema";
import { Estilos } from "../components/EstilosGlobais/Estilos";
import { Botao } from "../components/Botao/Botao";


function SolicitacaoSenha() {
  return (
    <ProvedorTema>
      <Estilos />
      <BaseLogin texto="Digite seu TIA ou DRT." tamanho="90vh" variante="body1" componente="h2">
        <CampoTexto label="TIA/DRT" />
        
        <div id="botoes" style={{width: "100%", display: "flex", justifyContent: "center", margin: "30px 0 0 0"}}>
          <div style={{width: "100%", display: "flex", justifyContent: "left"}}>
          <Botao text="Cancelar"/>
          </div>
          <div style={{width: "100%", display: "flex", justifyContent: "right"}}>
          <Botao text="Confirmar"/>
          </div>
        </div>

      </BaseLogin>
    </ProvedorTema>
  );
}

export default SolicitacaoSenha;
